<?php
  require '../include/config.php';
  //載入樣板
  //header
  require '../template/tp_site_header.php';
  //navbar
  require '../template/tp_header.php';
?>
<link type="text/css" rel="stylesheet" href="../css/bootstrap-lightbox.min.css"></link>
<div class="container whiteBg">
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT;?>">首頁</a> <span class="divider">/</span></li>
    <li><a href="<?php echo SITE_ROOT;?>seminar/">研習會活動</a> <span class="divider">/</span></li>
    <li class="active"> 活動花絮</li>
  </ul>
</div>
<div class="container whiteBg">
	<div class="row">
		<div class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_seminarSideMenuBar.php';?>
    </div>
   
		<div class="span9">
			<section id="first">
				<legend>
        中區水波拉提研習會<span class="seminarDate">2013/02/24</span>
      </legend>
				<ul class="thumbnails">
					<li class="span3">
							<img src="../img/seminar-1/1-small.jpg" alt="Reage 水波拉提研討會相片">
					</li>
					<li class="span3">
							<img src="../img/seminar-1/2-small.jpg" alt="Reage 水波拉提研討會相片">
					</li>
					<li class="span3">
							<img src="../img/seminar-1/3-small.jpg" alt="Reage 水波拉提研討會相片">
					</li>
					<li class="span3">
							<img src="../img/seminar-1/4-small.jpg" alt="Reage 水波拉提研討會相片">
					</li>
					<li class="span3">
							<img src="../img/seminar-1/5-small.jpg" alt="Reage 水波拉提研討會相片">
					</li>
					<li class="span3">
							<img src="../img/seminar-1/6-small.jpg" alt="Reage 水波拉提研討會相片">
					</li>
					<li class="span3">
							<img src="../img/seminar-1/7-small.jpg" alt="Reage 水波拉提研討會相片">
					</li>
					<li class="span3">
							<img src="../img/seminar-1/8-small.jpg" alt="Reage 水波拉提研討會相片">
					</li>
				</ul>
			</section>
		</div>
	</div>
	
	
</div>
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 活動花絮" );

      $('.sideSubMenu').find('li').eq(3).addClass('sideMenuActive');
		});
</script>