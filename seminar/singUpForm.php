<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>


<div class="container whiteBg">
	<ul class="breadcrumb">
  	<li><a href="#">首頁</a> <span class="divider">/</span></li>
  	<li><a href="#">研習會活動</a> <span class="divider">/</span></li>
    <li class="active"> 報名方式</li>
	</ul>
</div>
<div class="container whiteBg">
	<div class="row">
		<div id="contentSideMenuStyle" class="span3">
			<!--Sidebar Emnu-->
      <?php require'../template/tp_seminarSideMenuBar.php';?>
		</div>
		<div class="span9">
			<legend>
				報名方式
			</legend>
			<!--
			<div class="heroIntro2">
				<p>
					Reage 水波拉提術目前研習內容分成「臉部初階研習會」與「臉部進階研習會」，其內容如下：
				</p>
			</div>
		-->
			
			<div class="row">
				<div class="span6">
					<!---->
			<form action="../action/modify.php?type=sign_add" method="post" accept-charset="utf-8" class="form-horizontal">
				<fieldset>
					<div class="control-group">
						<label class="control-label">中文姓名：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入中文姓名" class="span4" name="chineseName" id="chineseName">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">英文名稱：</label>
						<div class="controls">
							<input type="text" placeholder="與護照英文名相同" class="span4" name="englidhName" id="englidhName">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">就職院所：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入就職院所名稱" class="span4" name="hospital" id="contactTel">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">電子信箱：</label>
						<div class="controls">
							<input type="email" placeholder="請輸入聯絡信箱" class="span4" name="email" id="contactEmail">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="text" placeholder="例：04-2326-8022" class="span4" name="telPhone" id="contactEmail">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">行動電話：</label>
						<div class="controls">
							<input type="tel" placeholder="例：0912345678" class="span4" name="cellPhone" id="contactEmail">
						</div>
					</div>
				<!--
					<div class="control-group">
						<label class="control-label">研習地點：</label>
						<div class="controls">
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
							  北區（桃園以北）
							</label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option2">
							  中區（新竹以南至嘉義）
							  </label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
							  南區（嘉義以南）
							  </label>
						</div>
					</div>
				-->
					<div class="control-group">
						<label class="control-label">報名研習會日期：</label>
						<div class="controls">
							<select name="optionMonth">
							  <option value="5/18-北區研習會">5/18-北區研習會</option>
							  <option value="6/23-中區研習會">6/23-中區研習會</option>
							  <option value="7/18-南區研習會">7/18-南區研習會</option>
							  <option value="8/18-北區研習會">8/18-北區研習會</option>
							  <option value="9/18-中區研習會">9/18-中區研習會</option>
							  <option value="10/18-南區研習會">10/18-南區研習會</option>
							  <option value="11/18-北區研習會">11/18-北區研習會</option>
							  <option value="12/18-中區研習會">12/18-中區研習會</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">其他備註：</label>
						<div class="controls">
							<textarea rows="3" class="span4" name="comment" id="comment"></textarea>
						</div>
					</div>

					<div class="control-group">
						<div class="controls">
							<hr />
							<button type="submit" class="btn pull-left">
								確認報名
							</button>
						</div>
					</div>

				</fieldset>
			</form>
			<!---->
				</div>
				<div class="span3">
					<h4>預定地點表</h4>
					<ul>
						<li>5/18-未定地</li>
						<li>6/23-未定地點</li>
						<li>7/23-未定地點</li>
						<li>8/23-未定地點</li>
						<li>9/23-未定地點</li>
						<li>10/23-未定地點</li>
						<li>11/23-未定地點</li>
						<li>12/23-未定地點</li>
					</ul>	
				</div>
			</div>

		</div>

	</div>

</div>
<?php
require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 醫學研習會" );
			$('.sideSubMenu').find('li').eq().addClass('sideMenuActive');
		});
</script>