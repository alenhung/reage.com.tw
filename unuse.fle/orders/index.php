<?php
require '../include/config.php';
//載入樣板
//header
require '../template/tp_header.php';
//navbar
require '../template/tp_navbar.php';
?>
<!--CONTENT START-->
<header class="siteHeader">
	<div id="container contentStart">

	</div>
</header>
<div class="container">
	<div class="row">
		<div class="span12">
			<div id="contactDIV">
				<h3>感謝您的訂購！</h3>
				<p>收到您的訂購需求後，我們將近快與您聯繫。</p>
			</div>
		</div>
	</div>
	<form action="action/modify.php?type=contact" method="post" accept-charset="utf-8" class="form-horizontal">
		<fieldset>
			<div class="row">
				<div class="span6">
					<legend>
						聯絡資訊
					</legend>
					
					<!--new -->
					<div class="control-group require" id="hospitalName">
						<label class="control-label">院所名稱：</label>
						<div class="controls ">
							<input type="text" placeholder="請輸入院所名稱" class="span4" name="companyName" id="companyName">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require" >
						<label class="control-label">聯絡人：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入聯絡人名稱" class="span4" name="contactName" id="contactName">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="tel" placeholder="請輸入聯絡電話" class="span4" name="contactTel" id="tel">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡信箱：</label>
						<div class="controls">
							<input type="email" placeholder="請輸入聯絡信箱" class="span4" name="contactEmail" id="email">
							<span class="help-inline" id="wrongEmail"></span>
						</div>
					</div>
					<div class="control-group require" >
						<label class="control-label">運送地址：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入運動地址" class="span4" name="contactName" id="contactName">
							<span class="help-inline"></span>
						</div>
					</div>
				</div>
				<div class="span6">	
					<legend>
						請選擇訂購項目
					</legend>
					<div class="control-group">
						<label class="control-label">產品系列：</label>
						<div class="controls">
							<select name="" id="depatrment-list">
								<option value="0">請選擇產品系列</option>
								<option value="1">Reage 逆齡系列</option>
								<option value="2">Drean UP 夢想系列</option>
							</select>
						</div>
					</div>
					<div class="control-group" id="product-list" style="display:none;">
						<label class="control-label">產品規格：</label>
						<div class="controls">
							<select name="" id="reage-list" style="display:none;">
								<option value="0">請選擇產品規格</option>
								<option value="r1">R1 (30G/30mm/30mm)</option>
								<option value="r2">R2 (30G/40mm/50mm)</option>
								<option value="r5">R5 (29G/60mm/90mm)</option>
							</select>

							<select name="" id="dream-list" style="display:none;">
								<option value="0">請選擇產品規格</option>
								<option value="d1">Mono-D1 (29G/25.4mm/30mm)</option>
								<option value="d2">Mono-D2 (29G/38mm/50mm)</option>
								<option value="t1">Torando-T1 (26G/38mm/50mm)</option>
								<option value="t2">Torando-T2 (26G/60mm/90mm)</option>
								<option value="t2">Torando-T3 (26G/90mm/135mm)</option>
								<option value="rose1">Rose-R1 (23G/60mm/90mm)</option>
								<option value="rose2">Rose-R2 (23G/90mm/120mm)</option>
							</select>
						</div>
					</div>
					<div class="control-group" id="product-quantity" style="display:none;">
						<label class="control-label">訂購數量：</label>
						<div class="controls">
							<input type="numbers" placeholder="請輸入訂購盒數" class="" name="" id="quantity"> <span class="remark"> (每盒一百隻)</span>
						</div>
					</div>
					<hr>
					<a href="#" class="btn pull-right" id="addItem" disabled>增加到訂單</a>
				</div>
				<div class="span12">
					<legend>您的訂購清單</legend>
					<div id="empty-cart"><h2 class="empty-cart-word">目前您的訂單是空的</h2></div>
					<div id="cart">
						<legend>
							選訂項目
						</legend>
					
						<table class="table table-striped table-hover">
							<tr>
								<td>Item</td>
								<td>Gauge</td>
								<td>Neddle Length</td>
								<td>Threads Length</td>
								<td>Quantity</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="span12">
					<hr />
					<div class="control-group">
						<div class="controls">
							
							<button type="submit" class="btn pull-right">送出</button>
							<span class="help-inline"></span>
						</div>
					</div>	
				</div>
			</div>
		</fieldset>
	</form>
	
	

</div>
<!--CONTENT END-->
<?php
require '../template/tp_footer.php';
?>
<script src="../js/jquery.numeric.js"></script>
<script>
$(document).ready(function(){
	$( 'title' ).html ( "<?php echo SITE_NAME;?> - 聯絡我們" );
	$('#tel').numeric();
	$('#email').change(function(){
		if(validateEmail($(this).val())){
			$('#email').parent().parent().removeClass('error');
			$('#wrongEmail').removeClass('inputWrong');
			$('#wrongEmail').text('');
		}else{
			
		}	
	});
	$('form').submit(function(){
		if(inputEmpty() >= 1){
			return false;
		}
		if(validateEmail($('#email').val())){
			return true;
			}else{
				$('#email').parent().parent().addClass('error');
				$('#wrongEmail').text('E-mail 格式錯誤');
			return false;
		};
	});

	$("#optionsRadios1").click(function(){
  		$('#userlocation').hide();
		$('#userPosition').hide();
		$('#hospitalName').show();
		$('#hospitalName').addClass('require');
  	});
	$("#optionsRadios2").click(function(){
  		$('#userlocation').show();
		$('#userPosition').show();
		$('#hospitalName').hide();
		$('#hospitalName').removeClass('require');
  	});
  	//
  	var $selectItem = 0 ;
  	var $selectQuantity = 0 ;
  	$('#depatrment-list').change(function(){
  		// alert(this.value);
  		switch (true) {
  			case (this.value == 0): 
  				$('#product-list').hide();
  				$('#addItem').attr('disabled',true);
  				$('#product-quantity').hide();
  				$('#quantity').val() =="";
  			break;
  			case (this.value == 1): 
  				$('#product-list').show();
  				$('#reage-list').show();
  				$('#dream-list').hide();
  				$selectItem = 0 ;
  				$('#addItem').attr('disabled',true);
  				$('#product-quantity').hide();
  				$('#quantity').val() =="";
  			break;
  			case (this.value == 2): 
  				$('#product-list').show();
  				$('#reage-list').hide();
  				$('#dream-list').show();
  				$selectItem = 0 ;
  				$('#addItem').attr('disabled',true);
  				$('#product-quantity').hide();
  				$('#quantity').val() =="";
  			break;
  			default:
  				$('#product-list').hide();
  				alert('操作錯誤！');
  		}
  	});
  	
  	$('#reage-list').change(function(){
  		if(this.value ==0){
  			$selectItem = this.value;
  			$('#addItem').attr('disabled',true);
  		}else{
  			$('#addItem').attr('disabled',false);
  			$('#product-quantity').show();
  			$selectItem = this.value;
  		}
  		// alert($selectItem);
  	});
  	$('#dream-list').change(function(){
  		if(this.value ==0){
  			$selectItem = this.value;
  			$('#addItem').attr('disabled',true);
  		}else{
  			$('#addItem').attr('disabled',false);
  			$('#product-quantity').show();
  			$selectItem = this.value;
  		}
  		// alert($selectItem);
  	});
  	$('#addItem').click(function(){
  		if($('#quantity').val() ==""){
  			alert('請輸入訂購盒數');
  			return false;
  		}else{
  			$selectQuantity = $('#quantity').val();
  		}
  		// alert($selectQuantity);
  	});
  	// $('#product-list').hide();
  	//$('#reage-list')
  	//$('#dream-list')
});
</script>