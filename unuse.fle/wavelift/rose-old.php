<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>
<header class="siteHeader">
	<div class="container">
		<img src="../img/site-indexlogo6.png" alt="" />
	</div>
</header>

<?php require '../template/tp_wavelift_navbar.php';?>

<div class="container">
	<h4 class="grey-line3 content-top">
		<span>適用於重度老化的患者，例如臉頰下垂嚴重、頸紋及雙下巴等</span>
	</h4>
	<div class="row">
		<div class="span12">
			<img src="../img/rose-1.png" alt="水波拉提-rose針頭" />
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<h1>玫瑰倒勾線 </h1>
			<p>目前市場目前僅此產品為PDO線倒勾設計。</p>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span12">
			<h1>雙側線性倒勾 物理性拉提極致指標</h1>
			<img src="../img/rose-2.png" alt="水波拉提-rose針頭" />
		</div>
	</div>
	<div class="row">
		<div class="span6">
			<div class="reage1">
				<h2>表裡一致 雙側倒勾物理拉提設計</h2>
				<p>內外一同擁有倒溝設計，雙側物理線性拉提加倍！</p>
				
			</div>
		</div>
		<div class="span6 imageside">
				<img src="../img/rose-3.png" alt="水波拉提-rose針頭" />
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span6">
			<img src="../img/kfda.png" alt="" />
		</div>
		<div class="span6 wave1">
			<h1>旋風登場</h1>
			<p>韓國KFDA第四級安全認證</p>
			<p>台灣衛生署醫療器械輸入許可證</p>
			<h3>更多資訊請洽本公司各區業務經理或來電洽詢</h3>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="span12">
	<h3>Rose (combi) 玫瑰倒勾線</h3>
	<table class="table table-striped table-hover">
		<tr>
			<td>item</td>
			<td>needle length</td>
			<td>threads length</td>
		</tr>
		<tr>
			<td>Rose1 (23G)</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
		<tr>
			<td>Rose2 (23G)</td>
			<td>90mm</td>
			<td>120mm</td>
		</tr>
		
	</table>
		</div>
	</div>
		
	</div>

<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(3)
</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 水波拉提" );
		});
</script>