<?php
require '../include/config.php';
//載入樣板
	//header
	require '../template/tp_header_type2.php';
	//navbar
	require '../template/tp_navbar_type2.php';
?>

<style type="text/css">
	b{color:red;};
</style>
<div class="container whiteBg blackTop">
	<div class="row contentBlockPage">
		<div class="span6">
			<form action="../action/modify.php?type=sign_add_1" method="post" accept-charset="utf-8" class="form-horizontal">
				<fieldset>
					<legend>
						線上報名表單
					</legend>
					<div class="control-group">
						
						<div class="controls">
							<input type="text" placeholder="研討會名稱" class="" name="seminarName" id="seminarName" style="display: none;" value="台中整外水波拉提術 研討會" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">中文姓名：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入中文姓名" class="" name="chineseName" id="companyName" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">英文名稱：</label>
						<div class="controls">
							<input type="text" placeholder="與護照英文名相同" class="" name="englidhName" id="contactName" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">就職院所：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入就職院所名稱" class="" name="hospital" id="contactTel" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">電子信箱：</label>
						<div class="controls">
							<input type="email" placeholder="請輸入聯絡信箱" class="" name="email" id="contactEmail" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="text" placeholder="例：04-2326-8022" class="" name="telPhone" id="contactEmail" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">行動電話：</label>
						<div class="controls">
							<input type="tel" placeholder="例：0912345678" class="" name="cellPhone" id="contactEmail" >
						</div>
					</div>
					<!--
					<div class="control-group">
						<label class="control-label">研習地點：</label>
						<div class="controls">
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
							  北區（桃園以北）
							</label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option2">
							  中區（新竹以南至嘉義）
							  </label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
							  南區（嘉義以南）
							  </label>
						</div>
					</div>
					-->
					<div class="control-group">
						<label class="control-label">其他備註：</label>
						<div class="controls">
							<textarea rows="3" class="" name="comment" id="comment" ></textarea>
						</div>
					</div>

					<div class="control-group">
						<div class="controls">
							<hr />
							<button type="submit" class="btn pull-right" >
								送出
							</button>
						</div>
					</div>

				</fieldset>
			</form>
		</div>
		<div class="span6">
			<legend>
				研習會內容
			</legend>
			<!-- <p>感謝您對於水波拉提術重視，我們非常重視您的參與意願！請您留下您的資料，我們將有專人與您聯繫。</p> -->
			
			<div class="seminarIntro">
				<h4>主講：賴炳文 整形外科醫師</h4>
				<p class="seminarIntroP">水波拉提創始醫師，藉由整形外科原理、<b>配合埋線技術</b>，達到水波拉提效果並減少瘀青，有別於其他埋線廠商。</p>
				<p class="seminarIntroP">有別於其他埋線廠商無醫師技術指導。</p>	
			</div>
			<div class="seminarContent">
				<ol>
					<li>
						研習時間：民國102年四月二十一日 （星期日） 下午一點三十分
					</li>
					<li>
						研習地點：彰化全方位診所地址（彰化市中正路二段825號） <a href="https://maps.google.com.tw/maps?q=%E5%BD%B0%E5%8C%96%E5%B8%82%E4%B8%AD%E6%AD%A3%E8%B7%AF%E4%BA%8C%E6%AE%B5825%E8%99%9F&ie=UTF8&ll=24.064197,120.534847&spn=0.017633,0.022466&sll=24.064516,120.534820&gl=tw&brcurrent=3,0x346938e5f385d5a1:0xe2e6f2408b5b01db,0,0x3469491eb5791475:0xd6e84b58ba347f27&hnear=500%E5%BD%B0%E5%8C%96%E7%B8%A3%E5%BD%B0%E5%8C%96%E5%B8%82%E4%B8%AD%E6%AD%A3%E8%B7%AF%E4%BA%8C%E6%AE%B5825%E8%99%9F&t=m&z=16">地圖</a>  
					</li>
					<li>
						參加資格：限醫師參加
					</li>
					<li>
						研討會內容：
						<ol>
							<li>水波拉提術原理介紹。</li>
							<li>水波拉提術操作技巧。</li>
							<li>現場DEMO操作教學，請自行準備 model。</li>
						</ol>
					</li>
					<li>
						收費方式：僅收線材費及教學費兩萬元（200支水波拉提線，demo用）
					</li>
					<li>
						洽詢專線：04-2326-0822 | 中區經理 張承洋 : 0930-868-482
					</li>
				</ol>
			</div>
			<hr />
			<a href="<?php echo SITE_ROOT;?>wavelift" class="btn btn-primary">關於水波拉提</a>
			<hr />
			
			<div class="info-content">
				<h1 class="oops">OOPS!</h1>
				<p class="">糟糕！來不及報名怎麼辦？沒關係請在這裡填寫研習意願登記表。</p>
				<a href="<?php echo SITE_ROOT;?>seminar/normal.php" class="btn btn-info">研習意願登記表</a>
			</div>
		
		</div>

	</div>

</div>
<?php
require '../template/tp_footer_type2.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 醫學研習會" );
		});
</script>