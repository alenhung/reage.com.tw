<?php
require '../include/config.php';
//載入樣板
//header
require '../template/tp_header.php';
//navbar
require '../template/tp_navbar.php';
?>
<header class="siteHeader HeaderBlock">
	<div class="container">
		<h1>醫學研習會</h1>
	</div>
</header>
<div class="container">
	<div class="hero-unit">
	  <h2>感謝您的報名</h2>
	  <p>我們已成功收到您的報名，我們將儘速與您聯繫。</p>
	  <p>
	    <a class="btn btn-primary" href="<?php echo SITE_ROOT?>">
	      返回浩善首頁
	    </a>
	  </p>
	</div>

</div>
<?php
require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 醫學研習會" );
		});
</script>