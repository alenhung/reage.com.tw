<?php
	
	$siteAdminUser    =   'siteAdminUser';    //聯絡我們表單
	//管理端路徑
	date_default_timezone_set('Asia/Taipei');
	$createDate = date ("Y-m-d H:i:s" , mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')));
	//定義IP
	$user_ip = $_SERVER["REMOTE_ADDR"];
	//抓session 數值。
	$createUser = $_SESSION["username"];
	//抓session 數值- LEVEL。
	$userLEVEL = $_SESSION["level"];
	//
    define(ADMIN_ROOT, 'http://haoshan.com.tw/siteAdmin/');
	 //
	define(ADD_USER_SUCCESS,"已成功新增會員！感謝您！");
	define(ADD_USER_ERROR,"新增會員失敗！請聯絡系統管理員！");
	define(ADD_LEVEL_ERROR,"您的權限不足！請聯絡系統管理員！");
	define(ADD_DATA_SUCCESS,"已成功新增資料！感謝您！");
	define(EDIT_DATA_SUCCESS,"已成功更新資料！感謝您！");
	//更新資料用
	define(ADD_DATA_ERROR,"新增資料失敗！請聯絡系統管理員！");
	define(DELETE_DATA_SUCCESS,"已成功刪除資料！感謝您！");
	define(DELETE_DATA_ERROR,"刪除資料失敗！請聯絡系統管理員！");
	//合作中心使用名詞
	define(CENTER_LOCATION_1, '北區');
	define(CENTER_LOCATION_2, '中區');
	define(CENTER_LOCATION_3, '南區');
    
?>