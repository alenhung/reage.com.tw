/**
 * @author Alen Huang
 */
$(document).ready(function(){
	$('#updateData').hide();
	$('#update').click(function(){
		$('#updateData').show();
		$('#viewData').hide();
	});
	$('#cancelUpdate').click(function(){
		$('#updateData').hide();
		$('#viewData').show();
	});
	
});

function siteAdminWhichNav(j){
	$('#EditNAV ul li').eq(j).addClass('active');
};