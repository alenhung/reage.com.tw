<?php
$type 	=  'center_view';
//
require "../../include/config.php";
require "../action/config.php";
require "../template/tp_header.php";
require "../template/tp_navbar.php";
?>

<div class="container siteStart">
	<?php
	require "tp_navbar.php";
	?>
	
	<?php $list = mysql_fetch_assoc($result) ?>
	<div class="btn-group pull-right">
	  <a href="center_list.php" class="btn">返回列表</a>
	  <a id="update" href="#" class="btn btn-primary">修改資料</a>
	  <a id="delete" href="../action/modify.php?type=center_delete&deleteID=<?php echo $list['id']; ?>" class="btn btn-danger">刪除此資料</a>
	</div>
	<!------------------------------------------------------------檢視資料------------------------------------------------------------>
	<div id="viewData" class="row">
		<div class="span12">
			<?php
						$centerPlace = $list['centerArea'];
						switch ($centerPlace) {
							case 0:
								$centerPlace = CENTER_LOCATION_1;
								break;
							
							case 1:
								$centerPlace = CENTER_LOCATION_2;
								break;
							
							case 2:
								$centerPlace = CENTER_LOCATION_3;
								break;
							
							default:
								
								break;
						}
						?>
			<legend><?php echo $centerPlace; ?> - <?php echo $list['centerName']; ?><small class="pull-right">建立日期： <?php echo $list['createTime']; ?> | 建立資料：<?php echo $list['createUser']; ?> </small></legend>
			<dl class="dl-horizontal">
				<?php
					//拆解字串成Array
					$checked_array =explode(',', $list['centerProduct']);
					/*
					$checked_array[0] = "Reage";
					$checked_array[1] = "Dr BE2";
					$checked_array[2] = "其他產品";
				
					 * 
					 */?>
			  <dt>郵遞區號</dt>
			  <dd><?php echo $list['centerZipcode']; ?></dd>
			  <dt>地址</dt>
			  <dd><?php echo $list['centerAddress']; ?></dd>
			  <dt>主治醫師</dt>
			  <dd><?php echo $list['centerDoctor']; ?></dd>
			  <dt>業務聯絡人</dt>
			  <dd><?php echo $list['centerContact']; ?></dd>
			  <dt>聯絡電話</dt>
			  <dd><?php echo $list['centerTEL']; ?></dd>
			  <dt>行動電話</dt>
			  <dd><?php echo $list['centercCell']; ?></dd>
			  <dt>電子郵件</dt>
			  <dd><?php echo $list['centerEmail']; ?></dd>
			  <dt>網站網址</dt>
			  <dd><?php echo $list['centerWebsite']; ?></dd>
			  <dt>購買產品</dt>
			  <dd>
			  	<?php 
			  	/*
			  	foreach ($checked_array as $ourProduct) {
				    echo $ourProduct,'<br />';
				}
				 * 
				 */
				  if (in_array(0, $checked_array)) echo 'Reage <br/>';
				  if (in_array(1, $checked_array)) echo 'DR BE2 <br/>';
				  if (in_array(2, $checked_array)) echo '其他產品 <br/>';
			   ?>
			   </dd>
			  <dt>備註</dt>
			  <dd><?php echo $list['centerComment']; ?></dd>
			  <!--
			  <dt>登入ip</dt>
			  <dd><?php echo $list['createIP']; ?></dd>
				-->
			</dl>
		</div>
	</div>
	<!------------------------------------------------------------修改資料------------------------------------------------------------>
	<div id="updateData">
		<form action="../action/modify.php?type=center_update&updateID=<?php echo $list['id']; ?>" method="post" accept-charset="utf-8">
			<fieldset>
				<h3>修改技術合作中心</h3>
			</fieldset>
			<div class="row">
				<div class="span6">
					<div class="control-group">
						<label class="control-label">所屬區域：</label>
						<div class="controls">
							<label class="radio">
								<?php
								$checkArea = $list['centerArea'];
								?>
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="0" <?php if ($checkArea == 0) echo 'checked'; ?>>
							  北區
							</label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="1" <?php if ($checkArea == 1) echo 'checked'; ?>>
							  中區
							  </label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="2" <?php if ($checkArea == 2) echo 'checked'; ?>>
							  南區
							  </label>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">中心名稱：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心名稱"  name="centerName" id="centerName" class="span5" value="<?php echo $list['centerName']; ?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">郵遞區號：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心郵遞區號"  name="centerZipcode" id="centerZipcode" class="span3" value="<?php echo $list['centerZipcode']; ?>" > |<a href="http://www.post.gov.tw/post/internet/f_searchzone/index.jsp?ID=12105" target="_blank">查詢郵遞區號</a>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">中心地址：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心地址"  name="centerAddress" id="centerAddress" class="span5" value="<?php echo $list['centerAddress']; ?>" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">主治醫師：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心主治醫師"  name="centerDoctor" id="centerDoctor" class="span5" value="<?php echo $list['centerDoctor']; ?>" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">聯絡人員：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心聯絡者"  name="centerContact" id="centerContact" class="span5" value="<?php echo $list['centerContact']; ?>" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">市內電話：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心市內電話(04-23260822#123)"  name="centerTEL" id="centerTEL" class="span5" value="<?php echo $list['centerTEL']; ?>" >
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="control-group">
						<label class="control-label">行動電話：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心行動電話"  name="centercCell" id="centercCell" class="span5" value="<?php echo $list['centercCell']; ?>" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">電子信箱：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心電子信箱"  name="centerEmail" id="centerEmail" class="span5" value="<?php echo $list['centerEmail']; ?>" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">網站網址：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心網站網址(http://reage.com.tw)"  name="centerWebsite" id="centerWebsite" class="span5" value="<?php echo $list['centerWebsite']; ?>" >
						</div>
					</div>
					<?php
					//拆解字串成Array
					$checked_array =explode(',', $list['centerProduct']);
					?>
					<div class="control-group">
						<label class="control-label">合作產品：</label>
						<div class="controls">
							<label class="checkbox">
								<input type="checkbox" name="centerProduct[]" id="centerProduct1" value="0" <?php if (in_array(0, $checked_array)) echo 'checked'; ?>/>
								水波拉提
							</label>
							<label class="checkbox">
								<input type="checkbox" name="centerProduct[]" id="centerProduct2" value="1" <?php if (in_array(1, $checked_array)) echo 'checked'; ?>/>
								DR B
							</label>
							<label class="checkbox">
								<input type="checkbox" name="centerProduct[]" id="centerProduct3" value="2" <?php if (in_array(2, $checked_array)) echo 'checked'; ?>/>
								others
							</label>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">其他備註：</label>
						<div class="controls">
							<textarea rows="3" class="span5" name="centerComment" id="centerComment"><?php echo $list['centerComment']; ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<hr />
			<div class="pull-right">
			<a href="#" id="cancelUpdate" class="btn ">取消修改</a>  ｜
			<button type="submit" class="btn btn-primary">確認修改</button>
			</div>
		</form>
	</div>
</div>
<?php
require "../template/tp_footer.php";
?>
<script>
	siteAdminWhichNav(0);
</script>