<?php
$type 	=  'user_view';
//
require "../../include/config.php";
require "../action/config.php";
//檢查使用者權限
require "../action/level_check.php";
require "../template/tp_header.php";
require "../template/tp_navbar.php";
?>

<div class="container siteStart">
	<?php
	require "tp_navbar.php";
	?>
	
	<?php $list = mysql_fetch_assoc($result) ?>
	<div class="btn-group pull-right">
	  <a href="user_list.php" class="btn">返回列表</a>
	  <!-- <a id="update" href="#" class="btn btn-primary">修改資料</a> -->
	  <a id="delete" href="../action/modify.php?type=user_delete&deleteID=<?php echo $list['id']; ?>" class="btn btn-danger">刪除此資料</a>
	</div>
	<!------------------------------------------------------------檢視資料------------------------------------------------------------>
	<div id="viewData" class="row">
		<div class="span12">
			<legend><?php echo $list['username']; ?><small class="pull-right">建立日期： <?php echo $list['createTime']; ?> | 建立資料：<?php echo $list['createUser']; ?> </small></legend>
			<dl class="dl-horizontal">
			  <dt>管理者帳號</dt>
			  <dd><?php echo $list['username']; ?></dd>
			  <dt>真實姓名</dt>
			  <dd><?php echo $list['realName']; ?></dd>
			  <dt>電子信箱</dt>
			  <dd><?php echo $list['email']; ?></dd>
			  <!--
			  <dt>登入ip</dt>
			  <dd><?php echo $list['createIP']; ?></dd>
				-->
			</dl>
		</div>
	</div>
	<!------------------------------------------------------------修改資料------------------------------------------------------------>
	<!--
	<div id="updateData">
		<form action="../action/modify.php?type=supplier_update&updateID=<?php echo $list['id']; ?>" method="post" accept-charset="utf-8">
			<fieldset>
				<h3>修改技術合作中心</h3>
			</fieldset>
			<div class="row">
			<div class="span12">
				<div class="control-group">
					<div class="controls">
						<input type="text" placeholder="管理者帳號"  name="username" id="loginName" class="span3" <?php echo $list['supplierName']; ?>>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="password" placeholder="管理者密碼"  name="password" id="loginPassword" class="span3" <?php echo $list['supplierName']; ?>>
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="password" placeholder="請在輸入一次密碼"  name="password2" id="loginPassword" class="span3" <?php echo $list['supplierName']; ?>>
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="text" placeholder="真實姓名"  name="realName" id="realName" class="span3" <?php echo $list['supplierName']; ?>>
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="email" placeholder="電子信箱"  name="email" id="email" class="span3" <?php echo $list['supplierName']; ?>>
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<select name="level">
						  <option value="1">資料維護管理員者</option>
						  <option value="2">廠商管理者</option>
						  <option value="3">網站管理者</option>
						</select>
					</div>	
				</div>
			</div>
		</div>

			<hr />
			<div class="pull-right">
			<a href="#" id="cancelUpdate" class="btn ">取消修改</a>  ｜
			<button type="submit" class="btn btn-primary">確認修改</button>
			</div>
		</form>
	</div>
	-->
</div>
<?php
require "../template/tp_footer.php";
?>

<script src="../../js/jquery.numeric.js"></script>
<script src="../../js/siteInclude.js"></script>
<script>
	siteAdminWhichNav(0);
	//表單驗證用
	$(document).ready(function() {
		// Stuff to do as soon as the DOM is ready;
		$('#supplierTEL').numeric();
		$('#supplierCell').numeric();
		$('#supplierZipcode').numeric();
		
		$('#supplierEmail').change(function(){
			if(validateEmail($(this).val())){
				$('#supplierEmail').parent().parent().removeClass('error');
				$('#wrongEmail').text('');
			}else{
				
			}	
		});
		$('form').submit(function(){
			if(inputEmpty() >= 1){
				return false;
			}
			if(validateEmail($('#supplierEmail').val())){
				return true;
				}else{
					$('#supplierEmail').parent().parent().addClass('error');
					$('#wrongEmail').text('E-mail 格式錯誤');
				return false;
			};s
		});
	});
</script>