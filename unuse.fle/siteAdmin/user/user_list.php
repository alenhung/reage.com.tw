<?php
$type 	=  'user_list';
//
require "../../include/config.php";
require "../action/config.php";
require "../template/tp_header.php";
require "../template/tp_navbar.php";
?>

<div class="container siteStart">
	<?php
	require "tp_navbar.php";
	?>
	
	<div class="row">
		<div class="span12" id="alertArea"></div>
		<div class="span12">
			<legend>使用者列表<small class="pull-right">共計 <?php echo $count?> 筆資料</small></legend>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>使用者名稱</th>
						<th>真實姓名</th>
						<th>電子郵件</th>
						<th>詳細資訊</th>
					</tr>
				</thead>
				<tbody>
					<?php while($list = mysql_fetch_assoc($result)) { ?>
					<tr>
						<td><?php echo $list['username']?></td>
						<td><?php echo $list['realName']?></td>
						<td><?php echo $list['email']?></td>
						<td><a class="btn btn-info" type="button"  href="user_view.php?userID=<?php echo $list['id']; ?>">詳細資料</a></td>
					</tr>
					<?php } ?>	
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
require "../template/tp_footer.php";
?>
<script>
	siteAdminWhichNav(0);
</script>