<?php
require "../include/config.php";
require "template/tp_header.php";
?>
<div class="container siteStart">
	<div class="hero-unit">
		<div class="row">
			<div class="span5 pull-left">
				<h2><?php echo SITE_NAME; ?></h2>
				<h3>網站控制後台</h3>
			</div>
			<div class="pull-right">
				<form action="include/check_user.php" method="post" accept-charset="utf-8" class="well">
					<fieldset>
						<h3>請先登入</h3>
					</fieldset>
					<div class="control-group">
						<div class="controls">
							<input type="text" placeholder="管理者帳號"  name="username" id="loginName" class="span3">
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<input type="password" placeholder="管理者密碼"  name="password" id="loginPassword" class="span3">
						</div>	
					</div>
					<hr />
					<button type="submit" class="btn">送出</button>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
require "template/tp_footer.php";
?>