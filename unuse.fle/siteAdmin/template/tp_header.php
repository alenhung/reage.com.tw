<!DOCTYPE html>
<html lang="zh-TW">
  <head>
    <meta charset="utf-8">
    <title><?php echo SITE_NAME;?> - 網站控制後台</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="haoshan.com.tw">
    <!-- Le styles -->
    <link href="<?php echo SITE_ROOT;?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo SITE_ROOT;?>css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo SITE_ROOT;?>css/style.css">
    <link rel="stylesheet" href="<?php echo SITE_ROOT;?>css/config.css">
    <link rel="stylesheet" href="<?php echo ADMIN_ROOT;?>css/style.css">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body>
