<?php
	require 'include/config.php';
	//載入樣板
	//header
	require 'template/tp_site_header.php';
?>
<?php
	//site header
	require 'template/tp_header.php';
?>
<!--CONTENT START-->

<link rel="stylesheet" href="<?php echo SITE_ROOT;?>css/featured-content-slider.css">

<div class="container sliderContent visible-desktop">
	<?php 
		require 'template/tp_indexSlider.php';
		//require 'template/tp_indexIntro.php';
	?>
</div>
<div class="container whiteBg hidden-desktop">
	<?php require 'template/tp_indexIntro.php';?>
</div>
<div class="container whiteBg blankHeight">
	
</div>

<!--CONTENT END-->


<?php
	require 'template/tp_footer.php';
?>

<!-- <script src="js/jquery.carouFredSel-6.2.0-packed.js"></script> -->
<!-- <script src="js/jqFancyTransitions.1.8.min.js"></script> -->
<script type="text/javascript" src="js/featured-content-slider/js/jquery-ui-1.9.0.custom.min.js" ></script>
<script type="text/javascript" src="js/featured-content-slider/js/jquery-ui-tabs-rotate.js"></script>
<script type="text/javascript">
	$('.carousel').carousel({
	  interval: 5000
	})

	$(document).ready(function() {
		//$('#indexSlideImage').hide();
		
		$("#featured").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);

	});
	

</script>