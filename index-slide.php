<?php
	require 'include/config.php';
	//載入樣板
	//header
	require 'template/tp_site_header.php';
?>
<?php
	//site header
	require 'template/tp_header.php';
?>
<!--CONTENT START-->

<div class="container whiteBg">
	<!---->
	<div id="featured" >
		<ul class="ui-tabs-nav">
	        <li class="ui-tabs-nav-item" id="nav-fragment-1"><a href="#fragment-1"><img src="images/image1-small.jpg" alt="" /><span>15+ Excellent High Speed Photographs</span></a></li>
	        <li class="ui-tabs-nav-item" id="nav-fragment-2"><a href="#fragment-2"><img src="images/image2-small.jpg" alt="" /><span>20 Beautiful Long Exposure Photographs</span></a></li>
	        <li class="ui-tabs-nav-item" id="nav-fragment-3"><a href="#fragment-3"><img src="images/image3-small.jpg" alt="" /><span>35 Amazing Logo Designs</span></a></li>
	        <li class="ui-tabs-nav-item" id="nav-fragment-4"><a href="#fragment-4"><img src="images/image4-small.jpg" alt="" /><span>Create a Vintage Photograph in Photoshop</span></a></li>
		</ul>

	    <!-- First Content -->
    <div id="fragment-1" class="ui-tabs-panel" style="">
			<img src="img/slider/1.jpg" alt="" />
			<div class="info" >
				<h2><a href="#" >來自最棒的Reage 水波拉提術</a></h2>
				<p>現代人，因為壓力、熬夜、作息不正常，加上長期使用3C商品，使得膠原蛋白加速流失，讓美麗一點一滴的消毀....<a href="#" >繼續閱讀</a></p>
			</div>
    </div>

    <!-- Second Content -->
    <div id="fragment-2" class="ui-tabs-panel ui-tabs-hide" style="">
			<img src="img/slider/2.jpg" alt="" />
			<div class="info" >
				<h2><a href="#" >來自最棒的Reage 水波拉提術</a></h2>
				<p>現代人，因為壓力、熬夜、作息不正常，加上長期使用3C商品，使得膠原蛋白加速流失，讓美麗一點一滴的消毀....<a href="#" >繼續閱讀</a></p>
			</div>
    </div>

    <!-- Third Content -->
    <div id="fragment-3" class="ui-tabs-panel ui-tabs-hide" style="">
			<img src="img/slider/3.jpg" alt="" />
			<div class="info" >
				<h2><a href="#" >來自最棒的Reage 水波拉提術</a></h2>
				<p>現代人，因為壓力、熬夜、作息不正常，加上長期使用3C商品，使得膠原蛋白加速流失，讓美麗一點一滴的消毀....<a href="#" >繼續閱讀</a></p>
			</div>
    </div>

    <!-- Fourth Content -->
    <div id="fragment-4" class="ui-tabs-panel ui-tabs-hide" style="">
			<img src="img/slider/4.jpg" alt="" />
			<div class="info" >
				<h2><a href="#" >來自最棒的Reage 水波拉提術</a></h2>
				<p>現代人，因為壓力、熬夜、作息不正常，加上長期使用3C商品，使得膠原蛋白加速流失，讓美麗一點一滴的消毀....<a href="#" >繼續閱讀</a></p>
			</div>
    </div>
	</div>
	<!---->
</div>

<?php
	require 'template/tp_footer.php';
?>

<!-- <script src="js/jquery.carouFredSel-6.2.0-packed.js"></script> -->
<!-- <script src="js/jqFancyTransitions.1.8.min.js"></script> -->
<!--Slider-->
<script type="text/javascript" src="js/featured-content-slider/js/jquery-ui-tabs-rotate.js"></script>
<script type="text/javascript" src="js/featured-content-slider/js/jquery-ui-1.9.0.custom.min.js" ></script>
<script type="text/javascript" charset="utf-8">
	

	//$('.navbar').height();
	$(document).ready(function() {

		$("#featured").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
		
		//$('#indexSlideImage').hide();
	});
	/*
	$('.carousel').carousel({
	  interval: 5000
	});
	$('#slideshowHolder').jqFancyTransitions({
	 	effect: 'curtain', // wave, zipper, curtain
		width: $ww-20, // width of panel
		height: $hh, // height of panel
		strips: 10, // number of strips
		delay: 5000, // delay between images in ms
		stripDelay: 50, // delay beetwen strips in ms
		titleOpacity: 0.7, // opacity of title
		titleSpeed: 1000, // speed of title appereance in ms
		position: 'alternate', // top, bottom, alternate, curtain
		direction: 'fountainAlternate', // left, right, alternate, random, fountain, fountainAlternate
		navigation: false, // prev and next navigation buttons
		links: false // show images as links
		
	 });

	$('#foo0').carouFredSel();
	$('.slideImage').width($ww);
	$('.slideImage').height($hh);
	 */
	//lider

		
</script>