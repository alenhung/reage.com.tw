ㄅ<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>
<div class="container whiteBg">
	<?php //require '../template/tp_breadcrumb.php'; ?>
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
    <li class=""> 案例介紹<span class="divider">/</span></li>
    <li class="active"> 身體</li>
  </ul>
</div>
<div class="container whiteBg">
	<div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_caseSideMenuBar.php';?>
    </div>
    <div class="span9">
    	<!--Body content-->
      <legend>身體</legend>
      <div class="heroIntro2">
          <p>
            內容整理中，敬請期待。
          </p>
      </div>
    </div>
	</div>
</div>
	
			
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 身體案例介紹" );

      $('.sideSubMenu').find('li').eq(4).addClass('sideMenuActive');
		});
</script>