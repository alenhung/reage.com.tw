<!DOCTYPE html>
<html lang="zh-TW">
  <head>
    <meta charset="utf-8">
    <title><?php echo SITE_NAME;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Keyword" content="ultra v lift,uvl,reage,v臉,3D埋線,3D拉提,4D埋線,4D拉提,拉提,線性拉提,埋線拉提,水波拉提,浩美優,瘦臉,除皺,增生,拉提,緊實,v臉提昇,下顎線條雕塑,雙下巴改善,木偶紋改善,蘋果肌拉提,抬頭文改善,頸紋改善,鼻部塑型,埋線廠商,埋線產品代理廠商,醫療器材代理廠商,相關術後代理廠商">
    <meta name="description" content="源自韓國2013醫美技術，使用比髮絲還細的PDO（polydioxanone）線，植入真皮層內，利用長達八個月的膠原蛋白增生反應，拉提鬆弛肌膚，緊實、拉提、除皺、增生一次達成！利用REAGE針頭極俱彈性的特性，可於皮膚真皮層內進行水波式入針，在鬆弛的真皮層裡形成網狀的支撐力，刺激更多膠原蛋白持續增生、拉提效果更加明顯，並可延長效果。在需要加強拉提的區域中，施行的醫師可以針對您的需求量身訂做，並可與其他醫美項目進行合併治療。細如髮絲的PDO（polydioxanone）線，原為心臟外科縫合使用，具有強大韌性並可被人體自然吸收分解的功能。真皮層內植入網狀的PDO線，利用異物反應，刺激膠原蛋白增生於縫線周圍組織，形成網狀支撐力提昇，改善鬆弛現象。PDO線將於八到十二個月內被人體自然吸收分解，但增生的膠原蛋白不會立即消失，改散鬆弛的提昇效果將繼續維持。">
    <meta name="author" content="haoshan.com.tw">
    <!-- Le styles -->
    <link rel="stylesheet" href="<?php echo SITE_ROOT;?>css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo SITE_ROOT;?>css/config.css">
    <link rel="stylesheet" href="<?php echo SITE_ROOT;?>css/bootstrap-responsive.css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo SITE_ROOT;?>css/reage.css">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    
    
    <!-- <link rel="stylesheet" href="<?php echo SITE_ROOT;?>js/featured-content-slider/style.css"> -->
  </head>
  <body>
