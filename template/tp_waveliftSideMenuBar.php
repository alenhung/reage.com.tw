<ul id="sideMenu">
  <li class="sideMenuTitle">REAGE水波拉提術指南</li>
  <ul id="menu1" class="sideSubMenu">
    <li class="">
      <a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 技術特點</a>
    </li>
    <li>
      <a href="<?php echo SITE_ROOT; ?>wavelift/object.php">適合對象</a>
    </li>
    <li class="">
      <a href="<?php echo SITE_ROOT; ?>wavelift/care.php">術後問題與護理</a>
    </li>
  </ul>
  <li class="sideMenuTitle">REAGE 線材規格</a>
  </li>
    <ul id="menu2" class="sideSubMenu">
      <li class="">
        <a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">單股線 Mono thread</a>
      </li>
      <li>
        <a href="<?php echo SITE_ROOT; ?>wavelift/tornado.php">雙股線 Tornado thread</a>
      </li>
      <li>
        <a href="<?php echo SITE_ROOT; ?>wavelift/rose.php">倒勾線 Rose thread</a>
      </li>
    </ul>
  <!--
  <li class="sideMenuTitle">技術團隊</li>
  <ul id="menu3" class="sideSubMenu">
    <li>
      <a href="<?php echo SITE_ROOT;?>wavelift/consultant.php">顧問醫師群</a>
    </li>
    <li>
      <a href="<?php echo SITE_ROOT;?>wavelift/certification.php">認證醫師群</a>
    </li>
  </ul>
-->
  <!--
  <li class="sideMenuTitle">媒體報導</a>
  </li>
    <ul id="menu4" class="sideSubMenu">
      <li>
        <a href="<?php echo SITE_ROOT;?>wavelift/report.php">媒體相關報導</a>
      </li>
    </ul>
  --> 
  <li class="sideMenuTitle">常見問題</li>
    <ul id="menu5" class="sideSubMenu">
      <li>
        <a href="<?php echo SITE_ROOT; ?>wavelift/qanda.php">常見問題</a>
      </li>
    </ul>
</ul>