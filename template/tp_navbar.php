

	<div class="headerRight">
		<span class="headerInfo"><a href="http://www.facebook.com/pages/%E6%B0%B4%E6%B3%A2%E6%8B%89%E6%8F%90/304628252973121" target="_blank">Reage 水波拉提 on Facebook</a></span> 
          <a href="http://www.facebook.com/pages/%E6%B0%B4%E6%B3%A2%E6%8B%89%E6%8F%90/304628252973121" target="_blank" ><img src="<?php echo SITE_ROOT;?>img/facebook-logo2.png" alt="水波拉提官方粉絲團" class="socialMediaImage"></a>
	</div>
	<div class="navbar navbar-haoshan">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="nav-collapse">
				<ul class="nav ">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" data-delay="1000" data-close-others="true">Reage <br>水波拉提術</a>
						<ul class="dropdown-menu span6">
							<li class="column-menu span3 firstcolumn">
		            <ul class="">
			              <li class="nav-header">Reage水波拉提術指南</li>
										<li class="spanMenu">
		                	<a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 技術特點</a>
		                </li>

			            	<li class="spanMenu">
											<a href="<?php echo SITE_ROOT; ?>wavelift/object.php">適合對象</a>
										</li>
										<li class="spanMenu">
											<a href="<?php echo SITE_ROOT; ?>wavelift/care.php">術後問題與護理</a>
										</li>
			              <li class="nav-header">Reage 線材規格</li>
			              <li class="spanMenu">
			              	<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">單股線 Mono thread</a>
			              </li>
			              <li class="spanMenu">
			              	<a href="<?php echo SITE_ROOT; ?>wavelift/tornado.php">雙股線 Tornado thread</a>
			              </li>
			              <li class="spanMenu">
			              	<a href="<?php echo SITE_ROOT; ?>wavelift/rose.php">倒勾線 Rose thread</a>
			            	<li>
		            </ul>
		        </li>
		        
		        <li class="column-menu span3">
		          
		          <ul class="">
		            <!--
		            <li class="nav-header">技術團隊</li>
								<li class="spanMenu">
									<a href="<?php echo SITE_ROOT;?>wavelift/consultant.php">顧問醫師群</a>
								</li>	
								<li class="spanMenu">
									<a href="<?php echo SITE_ROOT;?>wavelift/certification.php">認證醫師群</a>
								</li>
							-->
							<!--	
								<li class="nav-header">媒體報導</li>
								<li class="spanMenu">
									<a href="<?php echo SITE_ROOT;?>wavelift/report.php">媒體相關報導</a>
								</li>	
							-->
								<li class="nav-header">常見問題</li>
								<li class="spanMenu">
									<a href="<?php echo SITE_ROOT; ?>wavelift/qanda.php">常見問題</a>
								</li>	
		          </ul>
		        </li>
					</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" data-delay="1000" data-close-others="true">Gallery<br/>案例介紹</a>
					<ul class="dropdown-menu">
						<li>
							<a href="<?php echo SITE_ROOT; ?>case/topface.php">上臉部</a>
						</li>
						<li>
							<a href="<?php echo SITE_ROOT; ?>case/midface.php">中下臉部</a>
						</li>
						<li>
							<a href="<?php echo SITE_ROOT; ?>case/neck.php">雙下巴與頸部</a>
						</li>
						<li>
							<a href="<?php echo SITE_ROOT; ?>case/otherface.php">臉部其他部位</a>
						</li>
						<li>
							<a href="<?php echo SITE_ROOT; ?>case/body.php">身體</a>
						</li>
					</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" data-delay="1000" data-close-others="true">Seminar<br/>研習活動</a>
					<ul class="dropdown-menu">
						<li >
							<a href="<?php echo SITE_ROOT; ?>seminar/">課程安排</a>
						</li>
						<li >
							<a href="<?php echo SITE_ROOT; ?>seminar/calendar.php">研習會時間表</a>
						</li>
						<li>
							<a href="<?php echo SITE_ROOT; ?>seminar/singUp.php">報名方式</a>
						</li>
						<li >
							<a href="<?php echo SITE_ROOT; ?>event/">活動花絮</a>
						</li>
					</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle"  href="<?php echo SITE_ROOT;?>mystory" data-delay="1000" data-close-others="true">My Story<br/>素人故事</a>
					
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" data-delay="1000" data-close-others="true">Contact<br/>聯絡我們</a>
					<ul class="dropdown-menu">
						<li>
							<a href="<?php echo SITE_ROOT; ?>contact/medical.php">醫療人員諮詢</a>
						</li>
						<li>
							<a href="<?php echo SITE_ROOT; ?>contact/general.php">一般民眾諮詢</a>
						</li>
					</ul>
				</li>
				</ul>
			</div>
		</div>
	</div>
</div>