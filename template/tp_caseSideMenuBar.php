<ul id="sideMenu">
  <li class="sideMenuTitle">案例介紹</li>
  <ul class="sideSubMenu">
    <li class="">
      <a href="<?php echo SITE_ROOT; ?>case/topface.php">上臉部</a>
    </li>
    <li class="">
      <a href="<?php echo SITE_ROOT; ?>case/midface.php">中下臉部</a>
    </li>
    <li class="">
      <a href="<?php echo SITE_ROOT; ?>case/neck.php">雙下巴與頸部</a>
    </li>
    <li class="">
      <a href="<?php echo SITE_ROOT; ?>case/otherface.php">臉部其他部位</a>
    </li>
    <li class="">
      <a href="<?php echo SITE_ROOT; ?>case/body.php">身體</a>
    </li>
  </ul>
</ul>