<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';?>
<!--CONTENT START-->
<div class="container whiteBg">
	<ul class="breadcrumb">
  	<li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
  	<li><a href="#">聯絡我們</a> <span class="divider">/</span></li>
  	<li class="active">一般民眾諮詢</li>
	</ul>
</div>
<div class="container whiteBg">
	<div class="row">
		<div id="contentSideMenuStyle" class="span3">

			<!--Sidebar Emnu-->
      <?php require'../template/tp_contactSideMenuBar.php';?>
		</div>
		<div class="span9">
			<form action="../action/modify.php?type=contact_general" method="post" accept-charset="utf-8" class="form-horizontal">
				<fieldset>
					<legend>
						一般民眾諮詢專業
					</legend>
					<!--new -->
					<div class="control-group require" >
						<label class="control-label">聯絡人：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入聯絡人名稱" class="span4" name="contactName" id="contactName">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="tel" placeholder="請輸入聯絡電話" class="span4" name="contactTel" id="tel">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡信箱：</label>
						<div class="controls">
							<input type="email" placeholder="請輸入聯絡信箱" class="span4" name="contactEmail" id="email">
							<span class="help-inline" id="wrongEmail"></span>
						</div>
					</div>
					<!--user-->
					<div class="control-group" id="userlocation">
						<label class="control-label">所在區域：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入您的所在區域（例如：台北市）" class="span4" name="contactLocation" id="contactLocation">
							<span class="help-inline"></span>
						</div>
					</div>

					<div class="control-group" id="userPosition">
						<label class="control-label">您的問題：</label>
						<div class="controls">
							<label class="checkbox">
						    	<input type="checkbox" name="position[]" value="抬頭紋"> 抬頭紋
						     </label>
							
							<label class="checkbox">
						    	<input type="checkbox" name="position[]" value="眼尾下垂"> 眼尾下垂
						     </label>
							<label class="checkbox">
						    	<input type="checkbox" name="position[]" value="鼻部"> 鼻部
						     </label>
						  
							<label class="checkbox">
						    	<input type="checkbox" name="position[]" value="蘋果肌"> 蘋果肌
						     </label>
							<label class="checkbox">
						    	<input type="checkbox" name="position[]" value="法令紋、嘴角紋"> 法令紋、嘴角紋
						     </label>
						  <label class="checkbox">
						    	<input type="checkbox" name="position[]" value="臉部下垂"> 臉部下垂
						     </label>
							<label class="checkbox">
						    	<input type="checkbox" name="position[]" value="雙下巴"> 雙下巴
						     </label>
							<label class="checkbox">
						    	<input type="checkbox" name="position[]" value="頸部"> 頸部
						     </label>
							<label class="checkbox">
						    	<input type="checkbox" name="position[]" value="其他"> 其他
						     </label>
						</div>
					</div>
					<!--user-->
					<div class="control-group">
						<label class="control-label">諮詢內容：</label>
						<div class="controls">
							<textarea rows="3" class="span4" name="comment" id="comment"></textarea>
						</div>
					</div>
					<div class="control-group">
    					<div class="controls">
    						<hr />
    						<button type="submit" class="btn pull-left">送出</button>
    						<span class="help-inline"></span>
    					</div>
    				</div>	
				</fieldset>
			</form>
		</div>
	</div>
</div>
<!--CONTENT END-->
<?php
require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/jquery.numeric.js"></script>
<script>
$(document).ready(function(){
	$( 'title' ).html ( "<?php echo SITE_NAME;?> - 一般民眾諮詢專業" );
	$('.sideSubMenu').find('li').eq(1).addClass('sideMenuActive');
	$('#tel').numeric();
	$('#email').change(function(){
		if(validateEmail($(this).val())){
			$('#email').parent().parent().removeClass('error');
			$('#wrongEmail').removeClass('inputWrong');
			$('#wrongEmail').text('');
		}else{
			
		}	
	});
	$('form').submit(function(){
		if(inputEmpty() >= 1){
			return false;
		}
		if(validateEmail($('#email').val())){
			return true;
			}else{
				$('#email').parent().parent().addClass('error');
				$('#wrongEmail').text('E-mail 格式錯誤');
			return false;
		};
	});

	$("#optionsRadios1").click(function(){
  		$('#userlocation').hide();
		$('#userPosition').hide();
		$('#hospitalName').show();
		$('#hospitalName').addClass('require');
  	});
	$("#optionsRadios2").click(function(){
  		$('#userlocation').show();
		$('#userPosition').show();
		$('#hospitalName').hide();
		$('#hospitalName').removeClass('require');
  	});
});
</script>