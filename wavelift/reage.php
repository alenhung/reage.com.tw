<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
	//navbar
	//require '../template/tp_navbar.php';
?>
<!--
	<div class="container IntroDiv whiteBg">
		<img src="../img/wavelifting/intro-wavelifting.png" alt="" />
	</div>
-->
<div class="container whiteBg">
	<?php //require '../template/tp_breadcrumb.php'; ?>
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
    <li><a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 水波拉提術</a> <span class="divider">/</span></li>
    <li class=""> Reage 線材規格<span class="divider">/</span></li>
    <li class="active"> 單股線 Mono thread</li>
  </ul>
</div>
<div class="container whiteBg">
  <div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_waveliftSideMenuBar.php';?>
    </div>
    <div class="span9">
      <!--Body content-->
      <legend>
        Reage 單股線 Mono thread
      </legend>
      <div class="herointo">
        <p>適用於大部分的輕至中度老化客戶的需求，為Reage水波拉提基本用線。</p>
      </div>
      <div class="row">
      	<div class="span4">
      		<img src="../img/reage1.png" alt="水波拉提-reage針頭" />
      	</div>
      	<div class="span5">
      		<p id="mono1">細如髮絲的PDO（ploydioxanone）縫線，與專利彈性針頭完美的結合。</p>	
      	</div>
      </div>
      <!---->
			<div class="row">
				<div class="span3">
        <p id="mono2">
          特殊設計的針頭與彈性設計，有效降低治療後的局部瘀青現象。
        </p>    
        </div>
				<div class="span6"><img src="../img/wavelifting/reage-needleVS.jpg" alt=""></div>
			</div>
			<!---->
      <!--
      <div class="row">
        <div class="span4">
          <h3>極俱彈性與針頭特殊設計</h3>
        <p>傳統針頭的直線切割方式，雖便利入針順利但也容易刺破微血管造成出血。REAGE特殊導角切割與極俱彈性的設計，於施術過程中將有效避開血管，降低術後瘀清機率。因此成為水波拉提術不可或缺的利器。</p>
        </div>
        <div class="span5"><img src="../img/reage3.png" alt="水波拉提-reage針頭特殊設計" /></div>
      </div>
      -->
      <div class="row">
        <div class="span9">
          <h3>Reage Mono thread 規格表</h3>
          <table class="table table-striped table-hover">
            <tr>
              <td>item NO.</td>
              <td>Gauge</td>
              <td>USP</td>
              <td>Needle Length</td>
              <td>Thread Length</td>
            </tr>
            <tr>
              <td>R1</td>
              <td>30G</td>
              <td>7-0</td>
              <td>30mm</td>
              <td>30mm</td>
            </tr>
            <tr>
              <td>R2</td>
              <td>30G</td>
              <td>7-0</td>
              <td>40mm</td>
              <td>50mm</td>
            </tr>
            <tr>
              <td>R4</td>
              <td>29G</td>
              <td>6-0</td>
              <td>40mm</td>
              <td>50mm</td>
            </tr>
            <tr>
              <td>R5</td>
              <td>29G</td>
              <td>6-0</td>
              <td>60mm</td>
              <td>90mm</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 單股線 Mono thread" );

      $('#menu2 > li').eq(0).addClass('sideMenuActive');
		});
</script>