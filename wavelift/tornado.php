<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
	//navbar
	//require '../template/tp_navbar.php';
?>
<!--
	<div class="container IntroDiv whiteBg">
		<img src="../img/wavelifting/intro-wavelifting.png" alt="" />
	</div>
-->
<div class="container whiteBg">
	<?php //require '../template/tp_breadcrumb.php'; ?>
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
    <li><a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 水波拉提術</a> <span class="divider">/</span></li>
    <li class=""> Reage 線材規格<span class="divider">/</span></li>
    <li class="active"> 雙股線 Tornado thread</li>
  </ul>
</div>
<div class="container whiteBg">
  <div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_waveliftSideMenuBar.php';?>
    </div>
    <div class="span9">
      <!--Body content-->
      <legend>雙股線 Tornado thread</legend>
      <div class="herointo">
        <p>適用於中至重度老化患者，可提供更強效果，為Reage水波拉提進階用線。特別適合中下臉頰、雙下巴、法令紋、嘴角紋、頸部等部位使用。</p>
      </div>

        <h4>基本雙股線</h4>
      <div class="row">
        <div class="span4">
          <p>
            將兩條細如髮絲的PDO線互相纏繞組合而成，提供比一般單股線更優秀的支撐力。
          </p>
        </div>
        <div class="span5">
          <img src="../img/wavelifting/tornado-thread.png" alt="水波拉提-reage針頭" />
        </div>
      </div>
      <div class="row">
        <div class="span9">
          <h4>Reage Tornado thread 規格表</h4>
          <table class="table table-striped table-hover">
            <tr>
              <td>item NO.</td>
              <td>Gauge</td>
              <td>USP</td>
              <td>Needle Length</td>
              <td>Thread Length</td>
            </tr>
            <tr>
              <td>T1</td>
              <td>26G</td>
              <td>7-0  X 2</td>
              <td>38mm</td>
              <td>50mm</td>
            </tr>
            <tr>
              <td>T2</td>
              <td>26G</td>
              <td>7-0  X 2</td>
              <td>60mm</td>
              <td>90mm</td>
            </tr>
            <tr>
              <td>T3</td>
              <td>26G</td>
              <td>6-0 X 2</td>
              <td>90mm</td>
              <td>120mm</td>
            </tr>
          </table>
        </div>
      </div>
      <hr>
      <h4>雙股螺旋線</h4>
      
      <div class="row">

      	<div class="span4">
      		<p id="">已經互相纏繞的雙股PDO線再進化為螺旋纏繞於針頭的雙股螺旋線，可提供比一般單股PDO平滑線數倍效果。</p>	
      	</div>
        <div class="span5">
          <img src="../img/wavelifting/tornado-1.png" alt="水波拉提-reage針頭" />
        </div>
      </div>
      <!---->
      <!--
			<div class="row">

        <div class="span2"><img src="../img/tornado-icon1.png" alt=""></div>
				<div class="span7">
        <p id="reage2">
          雙股螺旋設計，目的在於提供更大體積刺激膠原蛋白增生，除了提供兩倍單股線的體積外，更纏繞螺旋於針頭上，比起線材螺旋入針後更加緊密，集中PDO刺激效果，為最強緊實、除皺的最佳利器。
        </p>    
        </div>
			</div>
    -->
      <div class="row">
        <div class="span9">
          <h4>Reage Tornado Screw thread 規格表</h4>
          <table class="table table-striped table-hover">
            <tr>
              <td>item NO.</td>
              <td>Gauge</td>
              <td>USP</td>
              <td>Needle Length</td>
              <td>Thread Length</td>
            </tr>
            <tr>
              <td>TS1</td>
              <td>26G</td>
              <td>7-0  X 2</td>
              <td>40mm</td>
              <td>50mm</td>
            </tr>
            <tr>
              <td>TS2</td>
              <td>26G</td>
              <td>7-0  X 2</td>
              <td>60mm</td>
              <td>90mm</td>
            </tr>
            <tr>
              <td>TS3</td>
              <td>26G</td>
              <td>6-0 X 2</td>
              <td>90mm</td>
              <td>135mm</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 雙股線 Tornado thread" );
      $('#menu2 > li').eq(1).addClass('sideMenuActive');
		});
</script>