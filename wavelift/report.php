<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>
<div class="container whiteBg">
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
    <li><a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 水波拉提術</a> <span class="divider">/</span></li>
    <li class=""> 媒體報導<span class="divider">/</span></li>
    <li class="active"> 媒體相關報導</li>
  </ul>
</div>
<div class="container whiteBg">
	<div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_waveliftSideMenuBar.php';?>
    </div>
    <div class="span9">
    	<!--Body content-->
      <legend>媒體相關報導</legend>
      <!---->
      <!--縮圖開始-->
      <ul class="thumbnails">
        <li class="span3">
          
            <img src="../img/magazine/drbeauty-02-small.jpg" alt="Click to view the lightbox">
          
        </li>
        <li class="span3">
          
            <img src="../img/magazine/drbeauty-03-small.jpg" alt="Click to view the lightbox">
          
        </li>
        <li class="span3">
          
            <img src="../img/magazine/drbeauty-04-small.jpg" alt="Click to view the lightbox">
          
        </li>
        <li class="span3">
            <img src="../img/magazine/psbeauty-01-small.jpg" alt="Click to view the lightbox">
          
        </li>
        <li class="span3">
          
            <img src="../img/magazine/psbeauty-02-small.jpg" alt="Click to view the lightbox">
          
        </li>
      </ul>
      <!--縮圖結束-->
      


      <!---->
    </div>
	</div>
</div>
	
			
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 媒體相關報導" );

      $('#menu4 > li').eq(0).addClass('sideMenuActive');
		});
</script>