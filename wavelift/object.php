<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
	//navbar
	//require '../template/tp_navbar.php';
?>
<!--
	<div class="container IntroDiv whiteBg">
		<img src="../img/wavelifting/intro-wavelifting.png" alt="" />
	</div>
-->
<div class="container whiteBg">
	<ul class="breadcrumb">
  	<li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
  	<li><a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 水波拉提術</a> <span class="divider">/</span></li>
  	<li class=""> Reage 水波拉提術指南<span class="divider">/</span></li>
    <li class="active"> Reage 適合對象</li>
	</ul>
</div>
<div class="container whiteBg">
  <div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_waveliftSideMenuBar.php';?>
    </div>
    <div class="span9">
      <!--Body content-->
      <legend>
        Reage 適合對象
      </legend>
          <div id="reageUserfor" class="row">
            <div  class="span5">
              <div id="usesfor">
                <h4>「Reage水波拉提」適合部位</h4>
                <ul>
                  <li class="blueList1">臉部鬆弛下垂</li>
                  <li class="blueList2">小V臉與下頜線</li>
                  <li class="blueList3">雙下巴與頸紋</li>
                  <li class="blueList4">前額與眉毛下垂</li>
                  <li class="blueList5">法令紋、嘴角紋等皺紋</li>
                  <li class="blueList6">鼻部</li>
                </ul>
              </div>
            </div>
            <div class="span4">
              <img src="../img/wavelifting/people.png" alt="Reage水波拉提 回溯時光的魔法" id="waveliftingUser"/>
            </div>
          </div>
       <!---->
       <hr>
       <!---->
          <div id="reageUserfor2" class="row">

            
            <div  class="span5">
              <div id="unusesfor">
                <h4>誰不適合「Reage水波拉提」？</h4>
                <ul>
                  <li class="redList1">治療區域有嚴重發炎現象時。</li>
                  <li class="redList2">有嚴重皮膚疾病時。</li>
                  <li class="redList3">對打針埋線類治療無法接受者。</li>
                  <li class="redList4">治療期望值不合理者。</li>
                </ul>
              </div>
            </div>
            <div class="span4">
              <img src="../img/wavelifting/people2.png" alt="Reage水波拉提 回溯時光的魔法" id="waveliftingUnuse"/>
            </div>
          </div>
       <!---->
    </div>
  </div>
</div>
			<!---->
      <!--
      <div class="row">
        <div class="span4">
          <h3>極俱彈性與針頭特殊設計</h3>
        <p>傳統針頭的直線切割方式，雖便利入針順利但也容易刺破微血管造成出血。REAGE特殊導角切割與極俱彈性的設計，於施術過程中將有效避開血管，降低術後瘀清機率。因此成為水波拉提術不可或缺的利器。</p>
        </div>
        <div class="span5"><img src="../img/reage3.png" alt="水波拉提-reage針頭特殊設計" /></div>
      </div>
      -->
      
<!--
<div class="container whiteBg">
	<div class="ripple">
		<img src="../img/wavelifting/ripple.jpg" alt="" />
		<hr />
	</div>
</div>
<div class="container whiteBg">
	<div class="row">
		<div class="span6">
			<img src="../img/wavelifting/retime.png" alt="Reage水波拉提 回溯時光的魔法" class="waveliftingImage"/>
		</div>
		<div id="retime" class="span6">
			<h2>倒轉時鐘 回溯時間的魔法</h2>
			<p>現代人，因為壓力、熬夜、作息不正常，加上長期使用3C商品，使得膠原蛋白加速流失，讓美麗一點一滴的消毀，即使是年輕的美眉，也容易出現鬆弛、下垂、皺痕、細紋等老化現象。若想在任何時候都能保持完美的青春漾貌，成為注目的焦點，新式隱型拉皮術「Reage水波拉提」可為你Hold住年輕、緊致的上揚線條。</p>			
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span7">
			<div class="">
				<h2>特殊水波式拉提法</h2>
				<p>利用REAGE針頭極俱彈性的特性，可於皮膚真皮層內進行水波式入針，在鬆弛的真皮層裡形成網狀的支撐力，刺激更多膠原蛋白持續增生、拉提效果更加明顯，並可延長效果。在需要加強拉提的區域中，施行的醫師可以針對您的需求量身訂做，並可與其他醫美項目進行合併治療。</p>
				
			</div>
		</div>
		<div class="span5">
				<img src="../img/wave-1.png" alt="" class="waveliftingImage"/>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span5">
			<img src="../img/skin.png" alt="水波拉提改善部位" class="waveliftingImage"/>
		</div>
		<div class="span7">
			<h2>作用原理</h2>
			<p>「Reage水波拉提」是利用一種可被人體吸收的PDO(polydioxanone)外科縫線，植入肌膚的真皮層內，藉由異物反應原理，長時間刺激膠原蛋白增生，並促進肌膚修復能力，強化鬆弛老化的肌膚彈性，回復年輕肌膚該有的緊緻線條，進而改善皺紋、下垂等老化現象。</p>
			<p>細如髮絲的PDO（polydioxanone）線，原為心臟外科縫合使用，具有強大韌性並可被人體自然吸收分解的功能。</p>
			<p>真皮層內植入網狀的PDO線，利用異物反應，刺激膠原蛋白增生於縫線周圍組織，形成網狀支撐力提昇，改善鬆弛現象。</p>
			<p>PDO線將於八到十二個月內被人體自然吸收分解，但增生的膠原蛋白不會立即消失，改散鬆弛的提昇效果將繼續維持。</p>
		</div>
	</div>
	<hr />
	<div id="reageUserfor" class="row">
		<div  class="span6">
			
			<div id="usesfor">
				<h2>「Reage水波拉提」適合部位與效果</h2>
				<ul>
					<li class="blueList1">改善臉部鬆弛下垂現象</li>
					<li class="blueList2">小V臉與下頜線拉提</li>
					<li class="blueList3">改善雙下巴與頸紋</li>
					<li class="blueList4">前額拉提與提眉</li>
					<li class="blueList5">法令紋、嘴角紋等皺紋填補</li>
					<li class="blueList6">鼻型修飾</li>
				</ul>
			</div>
		</div>
		<div class="span6">
			<img src="../img/wavelifting/people.png" alt="Reage水波拉提 回溯時光的魔法" class="waveliftingImage"/>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span12">
			<h2>雜誌報導</h2>
			<img src="../img/wavelifting/mag.png" alt="水波拉提醫美人雜誌報導2月號">
		</div>
	</div>
	<hr />
</div>
-->
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 適合對象" );
      $('#menu1 > li').eq(1).addClass('sideMenuActive');
		});
</script>